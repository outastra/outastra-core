#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/outastra.ico

convert ../../src/qt/res/icons/outastra-16.png ../../src/qt/res/icons/outastra-32.png ../../src/qt/res/icons/outastra-48.png ${ICON_DST}
